function redir(url) {
	window.location.href = url
}
function gotoPageFromInput(event, func) {
	if (event.keyCode == 13) {
		func()
	}
}


function gotoPage() {
	redir('page/' + document.querySelector('#pagenumber').value)
}

function gotoPageFromVerseKey() {
	let verseKey = document.querySelector('#versekey').value
	redir('page/' + verseKeyToPage[verseKey] + "#" + verseKey )
}

function gotoPageFromJuz() {
	let verseKey = juz[document.querySelector('#juznumber').value][0]
	if (verseKey.split(":")[1] == "1") {
		verseKey = verseKey.split(":")[0]
	}
	redir('page/' + verseKeyToPage[verseKey] )
}