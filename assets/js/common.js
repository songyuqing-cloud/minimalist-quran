let autoplay = true
let lastPlayedVerse = null

function playWord(wordKey) {
	new Audio("https://audio.qurancdn.com/wbw/" + wordKey + ".mp3").play()
}

function playBismillah() {
	new Audio("https://audio.qurancdn.com/Shatri/mp3/001001.mp3").play()
}

function playNextVerse(verseKey, previousVerse) {
	if (autoplay && (lastPlayedVerse == null || lastPlayedVerse == previousVerse)) {
		lastPlayedVerse = verseKey

		verseAudio = document.querySelector("#verseaudio-" + verseKey)
		verseAudio.currentTime = 0
		verseAudio.play()

		verseText = document.querySelector(`[id='${parseInt(verseKey.slice(3))}']`)
		verseText.scrollIntoView({"behavior": "smooth"})
	}
}

function resetLastPlayedVerse(e) {
	if (e.target.currentTime != e.target.duration) {
		lastPlayedVerse = null
	}
}

function toggleAutoplay(e) {
	autoplay = e.target.checked
	if (autoplay) {
	lastPlayedVerse = null
	}
}
