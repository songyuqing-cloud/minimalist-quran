module.exports = function(eleventyConfig) {

	eleventyConfig.addLayoutAlias('surah', 'layouts/surah.ejs');
	eleventyConfig.addLayoutAlias('page', 'layouts/page.ejs');

	eleventyConfig.addPassthroughCopy("assets");

  return {
    dir: {
	    input: "./",      // Equivalent to Jekyll's source property
		output: "./public", // Equivalent to Jekyll's destination property
		pathPrefix: "simple-quran"
    },
  };
};
